package com.cy.pj.sys.web.controller;

import com.cy.pj.common.util.PageUtil;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;
import com.cy.pj.sys.web.pojo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/log/")
@RestController
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;

    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable  Long id){
        return  new JsonResult(sysLogService.findById(id));
    }

    @DeleteMapping("{ids}")
    public JsonResult doDeleteById(@PathVariable Long... ids){
        sysLogService.deleteById(ids);
        return new JsonResult("delete ok!");
    }


    @GetMapping
    public JsonResult doFindLogs(SysLog syslog){
        return new JsonResult(PageUtil.startPage()//启动分页查询拦截
                .doSelectPageInfo(()->{//分页查询日志信息
                        //调用业务层方法执行查询，查询结果底层会存储到PageInfo对象
                        sysLogService.findLogs(syslog);
//                        String proxyCls = sysLogService.getClass().getName();
//                    System.out.println(proxyCls);//可查看当前AOP底层创建是子类还是兄弟类
                }));
        //假如应用了aop设计，sysLogService变量指向的是代理对象
    }
}
