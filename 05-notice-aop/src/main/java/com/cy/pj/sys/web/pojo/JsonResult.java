package com.cy.pj.sys.web.pojo;

/**
 * 所有的web项目，在设计响应数据时，都会有一个规范，
 * 比方说要响应给客户端什么数据，以什么格式进行响应，
 * 当前项目中，我们基于JsonResult对象封装服务端响应到客户端数据
 * 基于此对象封装服务器端响应到客户端数据，对于这样的对象通常包含三部分内容
 * 1）状态码
 * 2）消息
 * 3）数据
 */
public class JsonResult {
    /**状态码*/
    private Integer state=1;
    /**状态信息*/
    private String message="ok";
    /**响应数据，一般为查询操作结果*/
    private Object data;

    public JsonResult(){}

    public JsonResult(String message) {
        this.message = message;
    }
    public JsonResult(Integer state, String message){
        this(message);
        this.state=state;
    }

    public JsonResult(Object data) {
        this.data = data;
    }

    //当出现异常时,可以通过此构造方法对异常信息进行封装
    public JsonResult(Throwable exception){//new JsonResult(exception);
        this.state=0;
        this.message=exception.getMessage();
    }


    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
