package com.cy.pj.sys.service.aspect;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Date;

/**
 * 在Spring AOP应用中，基于@Aspect注解描述的类为切面类类型，此类当中要封装切入点以及通知方法的定义
 * 1）切入点：要切入扩展业务逻辑的一些目标方法集合（例如使用了特点注解描述的方法）
 * 2）通知：封装了扩展业务逻辑的一个方法（Spring中也可以使用特定注解进行描述）
 */
@Order(1)
@Slf4j
@Aspect//底层帮创建兄弟类或子类
@Component
public class SysLogAspect {

    /**
     * 基于@Pointcut注解定义切入点，这里的@annotation为一种切入点表达式
     * 表示由Required注解描述的方法为一个切入点方法，我们要在这样的方法上添加扩展业务逻辑
     */
    @Pointcut("@annotation(com.cy.pj.common.annotation.RequiredLog)")
    public void doLog(){};//此方法用于承载切入点的定义

    /**
     * 定义一个通知方法，这里使用@Around进行描述，表示在此方法内部可以调用目标方法，
     * 可以在目标方法执行之前或之后做一些拓展业务
     * @param pj 连接点，用于封装要执行的目标方法信息，
     *           ProceedingJoinPoint此类型参数只能定义在@Around注解描述的方法中
     * @return 这里的返回值一般为目标切入点方法的执行结果
     * @throws Throwable
     */
    @Around("doLog()")
    public Object doAround(ProceedingJoinPoint pj) throws Throwable {
        long t1 = System.currentTimeMillis();
        log.info("method start {}", t1);
        try {
            Object result = pj.proceed();//通过连接点调用目标方法(method.invoke)
            long t2 = System.currentTimeMillis();
            log.info("method end {}",t2);
            doSaveLogInfo(pj,(t2-t1),null);
            return  result;//当启动分页查询时候，返回值可以不写。因为pageHelper在数据持久层就已经查询到返回结果
        }catch (Throwable e) {
//            e.printStackTrace();
            log.info("method error {}",e.getMessage());
            long t3 = System.currentTimeMillis();
            doSaveLogInfo(pj,(t3-t1),null);
            throw e;
        }
    }



    @Autowired
    private SysLogService sysLogService;

    //记录用户行为日志
    private void doSaveLogInfo(ProceedingJoinPoint jp,long time,Throwable e) throws NoSuchMethodException, JsonProcessingException {
        //1.获取用户行为日志

        //1.1获取登录用户信息
        String username="tony";//后续是系统的登录用户

        //1.2获取登录用户的ip地址
        String ip="192.168.100.11";//后续可以借助三方工具类

        //1.3获取用户操作
        //1.3.1获取方法所在类的字节码对象(目标对象对应的字节码对象)
        Class<?> cls=jp.getTarget().getClass();

        //1.3.2获取注解描述的方法对象(字节码对象，方法名，参数列表)   //获取方法签名(通过此签名获取目标方法信息)
        Signature signature=jp.getSignature();
        System.out.println("signature="+signature.getClass().getName());//MethodSignatureImpl
        MethodSignature ms= (MethodSignature) signature;

        //Method targetMethod=methodSignature.getMethod();//cglib
        Method targetMethod=//cglib,jdk
                cls.getDeclaredMethod(ms.getName(),ms.getParameterTypes());
        System.out.println("targetMethod="+targetMethod);

        //1.3.3获取RequiredLog注解
        RequiredLog requiredLog=targetMethod.getAnnotation(RequiredLog.class);
        String operation=requiredLog.operation();

        //1.4 获取方法声明信息
        String method= cls.getName()+"."+targetMethod.getName();

        //1.5 获取方法执行时传入的实际参数
        Object[]args = jp.getArgs();//实际参数
        String params=new ObjectMapper().writeValueAsString(args);

        //1.6 获取状态信息
        Integer status=  e==null?1:0;

        //1.7 获取错误信息
        String error=  e==null?"":e.getMessage();


        //封装用户行为日志
        SysLog sysLog=new SysLog();
        sysLog.setUsername(username);
        sysLog.setIp(ip);
        sysLog.setCreatedTime(new Date());
        sysLog.setOperation(operation);
        sysLog.setMethod(method);
        sysLog.setParams(params);
        sysLog.setStatus(status);
        sysLog.setError(error);
        sysLog.setTime(time);
        //将日志写入到数据库
        log.info("user log {}",new ObjectMapper().writeValueAsString(sysLog));
        sysLogService.saveLog(sysLog);

//        new Thread(){//1M
//            @Override
//            public void run(){
//                sysLogService.saveLog(sysLog);
//            }
//        }.start();

//        对于如上形式的异步实现方式，在并发比较小的时候可以，但是一旦并发量比较大时，
//        反复创建线程和销毁线程会带来很大系统开销，进而影响整体性能。

    }


}
