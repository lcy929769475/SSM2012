package com.cy.pj.sys.service.impl;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.common.annotation.RequiredTime;
import com.cy.pj.sys.dao.SysLogDao;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户日志业务规范的实现
 */
@Transactional(readOnly = false,timeout = 60,
        rollbackFor = Throwable.class,
        isolation = Isolation.READ_COMMITTED)//隔脏读
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogDao sysLogDao;


    /**
     * @Async 注解描述的方法为一个异步切入点方法，这个方法执行时底层会在新的线程调用
     * @param sysLog
     * @Async 注解描述的方法其返回值类型建议为void，假如为非void你需要对方法返回值类型进行
     * 设计，比方说方法声明为Future<Integer> ,方法内部的返回值为 return new AsyncResult<Integer> (10)
     */
    //    @RequiredLog(operation = "日志添加")    不能加 不然会一直循环
    @Async
    @Override
    public void saveLog(SysLog sysLog) {
        System.out.println("save.log.thread->"+Thread.currentThread().getName());
        //模拟耗时操作
        try{Thread.sleep(10000);}catch (Exception e){}
//        try{ TimeUnit.SECONDS.sleep(10); }catch (Exception e){}  //二个都可以耗时
        sysLogDao.insertLog(sysLog);
    }

    @Transactional  //单体架构，要么删除都成功，要么都失败
    @RequiredLog(operation = "日志删除")
    @Override
    public int deleteById(Long... ids) {
        int rows = sysLogDao.deleteById(ids);
        throw new RuntimeException("删除失败");
    }

    //作业：springboot工程中 AOP方式的事务控制，去查@Transactional注解的应用

    @RequiredLog(operation = "日志查询基于id")
    @Override
    public SysLog findById(Long id) {
        SysLog log=sysLogDao.selectById(id);
        return log;

    }

    @Transactional(readOnly = true,timeout = 60,
            rollbackFor = Throwable.class,
            isolation = Isolation.READ_COMMITTED)//隔脏读
    @RequiredTime
    @RequiredLog(operation = "日志查询")//由此注解描述的方法为日志切入点方法
    @Override
    public List<SysLog> findLogs(SysLog sysLog) {
        System.out.println("log.find.thread->"+Thread.currentThread().getName());
       List<SysLog> list = sysLogDao.selectLogs(sysLog);
        return list;
    }
}
