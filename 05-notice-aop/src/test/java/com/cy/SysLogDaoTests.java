package com.cy;

import com.cy.pj.sys.dao.SysLogDao;
import com.cy.pj.sys.pojo.SysLog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@SpringBootTest
public class SysLogDaoTests {
    @Autowired
    /**@Autowired 有波浪线是因为在编译阶段SysLogDao接口的实现类确实还不存在，因为这个接口的实现类是在运行阶段*/
    private SysLogDao sysLogDao;
    @Test
    void testSelectLogs() throws ParseException {
        SysLog sysLog=new SysLog();
        sysLog.setUsername("admin");
        sysLog.setOperation("查询");
        sysLog.setStatus(1);
        String str="2020-05-01";
        SimpleDateFormat sdf=
                new SimpleDateFormat("yyyy-MM-dd");
        sysLog.setCreatedTime(sdf.parse(str));
        List<SysLog> logs=sysLogDao.selectLogs(sysLog);
        for(SysLog log:logs){
            System.out.println(log);
        }
    }
    @Test
    void testDeleteById(){
        sysLogDao.deleteById(1L,2L);
        System.out.println("delete ok");
    }

    @Test
    void testInsertLog(){
        SysLog sysLog = new SysLog();
        sysLog.setUsername("lcy");
        sysLog.setStatus(1);
        sysLog.setIp("192.168.11.19");
        sysLog.setOperation("插入公告");
        sysLog.setTime(System.currentTimeMillis());

        sysLogDao.insertLog(sysLog);
        System.out.println("insert ok");
    }

    @Test
    void selectById(){
        System.out.println(sysLogDao.selectById(11L));
    }
}
