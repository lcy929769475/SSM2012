package com.cy.java.basic.api;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicLongTests {
    public static void main(String[] args) {
        AtomicLong at=new AtomicLong(1);
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndIncrement());
        System.out.println(at.getAndDecrement());
        System.out.println(at.getAndDecrement());
    }

}
