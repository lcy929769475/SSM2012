package com.cy.java.basic.api.proxy;

import java.util.Arrays;

public class NoticeServiceImpl implements NoticeService{
    @Override
    public int deleteById(Long... id) {
        System.out.println("delete "+Arrays.toString(id));
        return id.length;
    }
}