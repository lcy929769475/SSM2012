package com.cy.java.basic.api;

import java.util.concurrent.TimeUnit;

public class ThreadTests02 {
    static String content;
    public static void main(String[] args) throws InterruptedException {
        new Thread(){
            @Override
            public void run() {
                content="helloWorld";
            }
        }.start();



        //上面新的线程有可能还没结束，不一定赋值，会空指针

        TimeUnit.SECONDS.sleep(1);//让主线程休眠一秒让CPU先执行上面线程
        System.out.println(content.toUpperCase());
    }
}
