package com.cy.java.basic.com.cy.java.oop.com.cy.api;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        List<Map<String,Object>> list=new ArrayList<>();
        Map<String,Object> map=new HashMap<>();
        map.put("id",100);
        map.put("title","TA");
        map.put("createdTime",System.currentTimeMillis());
        // System.out.println(map);
        list.add(map);//向list集合添加一个map时，添加的是什么？map的地址
        map.put("id",200);
        map.put("title","TB");
        map.put("createdTime",System.currentTimeMillis());
        // System.out.println(map);
        list.add(map);
        System.out.println(list);
        ;    }
}
