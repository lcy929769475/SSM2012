package com.cy.java.basic.com.cy.java.oop.com.cy.api;

public class IntegerTest {
    public static void main(String[] args) {
        Integer n1=100;//Integer.valueOf(100)
        Integer n2=100;
        Integer n3=200;
        Integer n4=200;
        System.out.println(n1==n2);//true
        System.out.println(n3==n4);//false
    }//整数池的应用和思考，底层设计模式享元模式
}
