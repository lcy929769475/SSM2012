package com.cy.java.basic.com.cy.java.oop.com.cy.api;


public class ThreadTest {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread 01 is running");
            }
        }).start();

        //lambda （当接口内部只有一个抽象方法时，可以通过lambda的方式简化构建匿名对象的过程）
        //例如 ， (参数列表)->{}
         new  Thread(()->{
             System.out.println("thread 02 is running");
         }).start();


         //方法内部只有一条语句的时候{}也可以去掉
         new Thread(()-> System.out.println("thread 03 is running")).start();
    }
}
