package com.cy.java.basic;

interface Shape{}

class Point{}

class Circle implements Shape{//is a
    private Point point;//has a
    private int r;
    public double doArea(){
        //Circle 与Math关系--->use a
        return Math.PI*r*r;
    }
}

class Cylinder extends Circle{} //is a



public class ClassRelationTests {
}
