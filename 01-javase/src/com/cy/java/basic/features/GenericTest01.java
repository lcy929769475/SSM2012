package com.cy.java.basic.features;

import java.util.ArrayList;
import java.util.List;

/**
 * 泛型？
 * 1)泛型是什么？
 * 泛型是编译时的一种类型，用于约束类中属性，方法参数，返回值类型，但运行时无效。
 * 2)泛型应用场景?(写框架-共性问题，写工具类，写通用方法)
 * 3)泛型应用类型?(类泛型，方法泛型)
 * 3.1)类泛型:语法-类名或接口名<泛型>(class ClassA<T>{},class ClassB<K,V,P,D>)
 * 类泛型约束类中实例属性，非静态方法参数，非静态返回值类型
 * 3.2)方法泛型: 语法-><泛型>方法返回值类型 方法名(参数列表)
 * 方法泛型应用最多的场景是静态方法(static <T>void print(T t){})
 * 4)泛型限定通配符
 * 4.1) ? 无界通配符
 * 4.2) ? super    下界限定通配符
 * 4.3) ? extends 上界限定通配符
 */
public class GenericTest01 {
    /**类泛型*/
    static class Container<E>{//new Container<String>()
        void add(E t){}
        E get(){return null;}
    }
    /**泛型方法，? extends 通常应用于获取值*/
    static <T extends Number>void print(T t){}
    /**
     * 下界限定通配符
     */
    static void show(List<? super String> t){}

    public static void main(String[] args) {
        print(100);//Integer

        List<String> list1=new ArrayList<>();
        List<Object> list2=new ArrayList<>();
        List<CharSequence> list3=new ArrayList<>();
        show(list1);
        show(list2);
        show(list3);
        List<Integer> list4=new ArrayList<>();
        List<Long> list5=new ArrayList<>();
        display(list4);
        display(list5);
    }

    /**
     * 限定上届通配符
     * @param t
     */
    static void display(List<? extends Number> t){}





}