package com.cy.pj.sys.web.interceptor;


import com.cy.pj.common.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

/**
 * SpringWeb模块定义的拦截器，其接口规范为HandlerInterceptor
 * 当前项目中我们要通过此拦截器对相关资源进行访问时间上的限制
 */
@Slf4j //lombok会在当前java文件编译成class文件时，会自动在类文件中添加一个log对象
public class TimeAccessInterceptor implements HandlerInterceptor {

    // private static final Logger log = LoggerFactory.getLogger(TimeAccessInterceptor.class);


    /**preHandle方法会在目标handler方法执行之前进行访问拦截
     * 方法返回值为true时表示请求放行，false表示请求到此结束，
     * 不再去执行handler中的方法
     * */

    /**
     * 后端handler方法之前执行
     * @param request 请求对象
     * @param response 响应对象
     * @param handler 目标处理器对象
     * @return 表示是否对请求放行，是否继续执行后续handler方法
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LocalTime now = LocalTime.now();
        // LocalDateTime 包含年月日和时间  LocalTime只包含时间
        int hour = now.getHour();
        log.info(" ==preHandler== hour {}", hour);
        if (hour>=21 || hour<9){
            throw new ServiceException("请在早上九点后或者晚上20点前进行访问！");
        }
        return true;//true表示放行 false表示拦截，请求到此结束
    }
}
