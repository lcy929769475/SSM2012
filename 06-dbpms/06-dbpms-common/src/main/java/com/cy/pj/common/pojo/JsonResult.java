package com.cy.pj.common.pojo;

import com.cy.pj.common.util.PageUtil;
import com.github.pagehelper.ISelect;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 所有的web项目，在设计响应数据时，都会有一个规范，
 * 比方说要响应给客户端什么数据，以什么格式进行响应，
 * 当前项目中，我们基于JsonResult对象封装服务端响应到客户端数据
 * 基于此对象封装服务器端响应到客户端数据，对于这样的对象通常包含三部分内容
 * 1）状态码
 * 2）消息
 * 3）数据
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonResult implements Serializable {
    private static final long serialVersionUID = -8416999444660048022L;
    /**状态码*/
    private Integer state=1; //1-ok , 0-Error;
    /**状态信息*/
    private String message="Ok";
    /**响应数据，一般为查询操作结果*/
    private Object data;

    public JsonResult(String message) {
        this.message = message;
    }
    public JsonResult(Integer state, String message){
        this(message);
        this.state=state;
    }

    public JsonResult(Object data) {
        this.data = data;
    }

    //当出现异常时,可以通过此构造方法对异常信息进行封装
    public JsonResult(Throwable exception){//new JsonResult(exception);
        this.state=0;
        this.message=exception.getMessage();
    }

    public JsonResult(ISelect select){
        this.data= PageUtil.startPage().doSelectPageInfo(select);
    }

}
