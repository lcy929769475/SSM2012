package com.cy.pj.sys.web.config;

import com.cy.pj.sys.web.interceptor.TimeAccessInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 定义spring web模块配置类
 */
@Configuration //此注解为spring中的一个配置类bean对象
public class SpringWebConfig implements WebMvcConfigurer {
    //配置 spring mvc 拦截器
    //注册拦截器，并且设置要拦截的路径,此方法会在项目启动时就会调用

//    @Autowired
//    private TimeAccessInterceptor timeAccessInterceptor;

    /**
     * 注册拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(
                new TimeAccessInterceptor()).addPathPatterns("/notice/*");
    }
}
