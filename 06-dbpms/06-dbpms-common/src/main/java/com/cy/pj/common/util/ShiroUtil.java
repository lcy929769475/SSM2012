package com.cy.pj.common.util;

import org.apache.shiro.SecurityUtils;

public class ShiroUtil {
    public static <T>T getUser(){
        return (T) SecurityUtils.getSubject().getPrincipal();
    }
}
