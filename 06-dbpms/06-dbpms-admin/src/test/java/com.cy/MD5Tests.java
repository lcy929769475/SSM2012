package com.cy;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@SpringBootTest
public class MD5Tests {
    @Test
    public void md5(){
        String pwd="1234567";
        SimpleHash sh = new SimpleHash("MD5",pwd);
        pwd = sh.toHex();
        System.out.println(pwd);//fcea920f7412b5da7be0cf42b8c93759
        String salt = UUID.randomUUID().toString();
        System.out.println(salt);//64121651-63f0-4602-930b-21d986357d26
        SimpleHash sh1= new SimpleHash("MD5",pwd,salt,1);
        System.out.println(sh1);
        pwd = sh1.toHex();
        System.out.println(pwd);//156694538ff6d3add30355a5678c45fe
    }

    @Test
    void MD5Test02() throws NoSuchAlgorithmException {
        String pwd="123456";
        MessageDigest md= MessageDigest.getInstance("MD5");
        byte[] array=md.digest(pwd.getBytes());
        System.out.println(array.length);//16byte==128bit
        //为了便于展示和读写一般将128位的二进制数转换成32位16进制数
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<array.length;i++){
            String s=Integer.toHexString(array[i]&0xFF);
            if(s.length()==1){
                s="0"+s;
            }
            sb.append(s);
        }
        System.out.println(sb.toString());//e10adc3949ba59abbe56e057f20f883e
    }

    @Test
    void testGeneratorKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator= KeyGenerator.getInstance("AES");
        SecretKey secretKey = keyGenerator.generateKey();
        String key= Base64.encodeToString(secretKey.getEncoded());
        System.out.println(key);
    }


}
