package com.cy.pj.sys.web.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.service.SysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user/")
@RestController
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @RequiresPermissions("sys:user:view")
    @GetMapping
    public  JsonResult doFindUsers(SysUser entity){
        return new JsonResult(()->sysUserService.findUsers(entity));
        //结果交给谁？  ：DispatcherServlet
    }

    @PostMapping
    public JsonResult doSaveUser(@RequestBody SysUser entity){
        sysUserService.saveUser(entity);
        return new JsonResult("save ok");
    }

    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable Integer id){
        return new JsonResult(sysUserService.findById(id));
    }

    @PutMapping//   @PutMapping  应用于少量数据的更新
    public JsonResult doUpdateUser(@RequestBody SysUser entity){
        sysUserService.updateUser(entity);
        return new JsonResult("update ok");
    }

    @RequiresPermissions("sys:user:update")
    @PatchMapping("{id}/{valid}")
    //少量数据的更新可使用 Patch 请求,当然使用 put 也可以.
    public JsonResult doValidById(@PathVariable Integer id,
                                  @PathVariable Integer valid ){

        sysUserService.validById(id, valid);
        return new JsonResult("update ok");
    }

    @GetMapping("login/{username}/{password}")
    public JsonResult doLogin(@PathVariable String username,@PathVariable String password){
        //控制层方法拿到用户名和密码以后要将这个信息提交给谁?(shiro)
        //通过谁提交？Subject
        //如何获取Subject?参考官方或源代码
        Subject subject= SecurityUtils.getSubject();
        //如何提交用户和密码？(调用subject对象的login方法)
        //为什么要构建UsernamePasswordToken对象？因为login方法需要
        //把账号密码封装到一个令牌里，在把令牌交给subject
        UsernamePasswordToken  token = new UsernamePasswordToken(username,password);
        token.setRememberMe(true);//设置记住我
        subject.login(token);//提交给shiro的securityManager
//        return  new JsonResult("login ok");
        JsonResult jr = new JsonResult();
        jr.setMessage("login ok");
        jr.setData(username);
        return jr;
    }

}
