package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = false,
        rollbackFor = Throwable.class,
        isolation = Isolation.READ_COMMITTED)
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuDao sysMenuDao;

    @Transactional(readOnly = true)
    @Override
    public List<SysMenu> findMenus() {
        //方法1(重点)
        return sysMenuDao.selectMenus();

        //方法2(拓展)
        //查找所有菜单信息：select * from sys_menus
//        List<SysMenu> menus = sysMenuDao.selectMenus();
        //在业务层对数据进行封装（一级菜单 二级菜单 三级菜单...），递归算法
//        menus.stream().filter().collect()
    }

    @Transactional(readOnly = true)
    @Override
    public List<Node> findMenuTreeNodes() {
        return sysMenuDao.selectMenuTreeNodes();
    }

    @Transactional(readOnly = true)
    @Override
    public SysMenu findById(Integer id) {
        return sysMenuDao.selectById(id);
    }

    @Override
    public int saveMenu(SysMenu menu) {
        return sysMenuDao.insertMenu(menu);
    }

//  @ZeroValidResult （可以自定义一个注解来校验结果是否返回为0）
    @Override
    public int updateMenu(SysMenu menu) {
        int rows =sysMenuDao.updateMenu(menu);
        if(rows==0)
            throw new ServiceException("记录可能不存在了~");
        return rows;
    }
}
