package com.cy.pj.sys.web.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.common.util.PageUtil;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/role/")
@RestController
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;

    @GetMapping
    public JsonResult doFindRoles(SysRole entity){
//        return  new JsonResult(PageUtil.startPage()
//                .doSelectPageInfo(()->sysRoleService.findRoles(entity)));
        return  new JsonResult(()->sysRoleService.findRoles(entity));
    }

    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable Integer id){
        return new JsonResult(sysRoleService.findById(id));
    }

    @GetMapping("checkRoles")
    public JsonResult doFindCheckRoles(){
        return new JsonResult(sysRoleService.findCheckRoles());
    }

    @PostMapping
    public JsonResult doSaveRole(@RequestBody  SysRole entity){
        sysRoleService.saveRole(entity);
        return new JsonResult("save ok~");
    }

    @PutMapping
    public JsonResult doUpdate(@RequestBody SysRole entity){
        sysRoleService.updateRole(entity);
        return new JsonResult("update ok~");
    }

}
