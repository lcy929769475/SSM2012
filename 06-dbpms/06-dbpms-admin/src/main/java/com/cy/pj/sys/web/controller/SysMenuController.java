package com.cy.pj.sys.web.controller;

import com.cy.pj.common.pojo.JsonResult;
import com.cy.pj.sys.pojo.SysMenu;
import com.cy.pj.sys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 创建SysMenuController类型，通过此对象处理客户端请求？
 * 1）处理什么请求？
 * 1.1）Get
 * 1.1.1）url:/menu/
 * 1.1.2) url:/menu/{id}
 * 1.2）Post
 * 1.2.1) url:/menu/
 * 1.3）Put
 * 1.3.1) url:/menu/
 * 1.4）Delete
 * 1.4.1) url:/menu/{id}
 * 2) 参数处理
 * 2.1） @PathVariable
 * 2.2） @RequestBody (id:10,name:"",...)
 * 2.3） @RequestParam ("/?id=10&...")
 * 3）响应什么数据？
 * 3.1) JsonResult
 * 3.2) ...
 * 4）异常数据如何处理？
 * 4.1) Controller 方法内部(每个方法内部都定义异常处理，try{}catch{})
 * 4.2) Controller 类的内部(Controller类的内部定义专门的异常处理，@ExceptionHandler)
 * 4.3) Controller 类的外部定义了全局异常处理（@RestControllerAdvice，..推荐）
 * 5)控制Controller方法的调用？(HandlerInterceptor)
 *
 */

@RequestMapping("/menu/")
@RestController
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 查询所有菜单以及菜单对应的上级菜单名称
     * @return
     * url: http://ip:port/menu
     */
    @GetMapping
    public JsonResult doFindMenus(){
        return new JsonResult(sysMenuService.findMenus());
    }

    /**
     * 基于id查询菜单信息  (http://ip:port/menu/8)
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable Integer id){
        return new JsonResult(sysMenuService.findById(id));
    }

    /**
     * 获取菜单节点信息
     * http://ip:port/menu/treeNodes
     * @return
     */
    @GetMapping("treeNodes")
    public JsonResult doFindMenuTreeNodes(){
        return new JsonResult(sysMenuService.findMenuTreeNodes());
    }


    /**
     * 添加菜单信息(http://ip:port/menu)
     * @param sysMenu {name:....,url:...,sort:..,...}
     * @return
     */
    @PostMapping
    public  JsonResult doSaveMenu(@RequestBody SysMenu sysMenu){
        sysMenuService.saveMenu(sysMenu);
        return new JsonResult("save ok!");
    }


    /**
     * 更新菜单信息(http://ip:port/menu)
     * @param sysMenu {id:10,name:....}
     * @return
     */
    @PutMapping
    public JsonResult doUpdateMenu(@RequestBody SysMenu sysMenu){
        sysMenuService.updateMenu(sysMenu);
        return new JsonResult("update ok!");
    }
}

/**
 * 拓展业务：
 * 1.组织管理（部门管理）
 * 2.商品分类（一级分类，二级分类，三级分类，...）
 * 3.文章分类（小说->武侠->年代->...）
 */
