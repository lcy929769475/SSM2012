package com.cy.pj.sys.service.impl;

import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.pojo.CheckBox;
import com.cy.pj.sys.dao.SysRoleDao;
import com.cy.pj.sys.dao.SysRoleMenuDao;
import com.cy.pj.sys.pojo.SysRole;
import com.cy.pj.sys.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色业务对象
 */
@Transactional(rollbackFor = Throwable.class,timeout = 60,
        readOnly = false,isolation = Isolation.READ_COMMITTED)
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    @Transactional(readOnly = true)
    @Override
    public List<SysRole> findRoles(SysRole entity) {
        return sysRoleDao.selectRoles(entity);
    }

    @Transactional(readOnly = true)
    @Override
    public SysRole findById(Integer id) {
        //多表关联查询
        SysRole sysRole = sysRoleDao.selectById(id);
        if(sysRole==null)
            throw new ServiceException("记录可能不存在");
        //单表多次查询
//      SysRole sysRole=sysRoleDao.selectById(id);//select * from sys_roles where id=#{id}
//      List<Integer> menuIds=sysRoleMenuDao.selectMenuIdsByRoleId(id);//select menu_id from sys_role_menus where role_id=#{id}
//      sysRole.setMenuIds(menuIds);
//      return sysRole;
        return sysRole;
    }

    @Override
    public int saveRole(SysRole entity) {
        //新增角色自身信息
        int rows = sysRoleDao.insertRole(entity);
        //新增角色菜单关系数据
        sysRoleMenuDao.insertRoleMenus(entity.getId(), entity.getMenuIds());
//        throw  new ServiceException("save error");   抛出一个异常试试事务有没有回滚
        return rows;
    }


    @Override
    public int updateRole(SysRole entity) {
        //更新角色自信信息
        int rows=sysRoleDao.updateRole(entity);
        //判断
        if(rows==0){
            throw new ServiceException("记录可能已经不存在");}
        //更新角色菜单关系数据
        //删除原有关系
        sysRoleMenuDao.deleteByRoleId(entity.getId());
        //添加新的关系
        sysRoleMenuDao.insertRoleMenus(entity.getId(), entity.getMenuIds());

        return rows;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CheckBox> findCheckRoles() {
        return sysRoleDao.selectCheckRoles();
    }
}
