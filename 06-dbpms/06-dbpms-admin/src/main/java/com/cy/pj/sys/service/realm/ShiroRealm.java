package com.cy.pj.sys.service.realm;

import com.cy.pj.sys.dao.SysMenuDao;
import com.cy.pj.sys.dao.SysUserDao;
import com.cy.pj.sys.pojo.SysUser;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * 定义shiro realm 对象 基于此对象获取用户认证和授权信息
 * 假如将来你的项目只做认证，只用继承AuthenticatingRealm对象即可
 */
public class ShiroRealm extends /*AuthenticatingRealm（只有认证没有授权）*/ AuthorizingRealm {
    @Autowired
    private SysUserDao sysUserDao;

    /**此方法负责获取并封装认证信息*/

    /**
     * @param authenticationToken 为封装了客户端认证信息的一个令牌对象
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取客户端提交的用户名
        UsernamePasswordToken upToken = (UsernamePasswordToken) authenticationToken;
        String username = upToken.getUsername();
        //基于用户名查询用户信息
        SysUser user = sysUserDao.selectUserByUsername(username);
        //判断用户是否存在
        if(user==null) throw new UnknownAccountException();
        //判断用户是否被禁用
        if(user.getValid()==0) throw new LockedAccountException();

        //封装用户信息
        ByteSource credentialsSalt =ByteSource.Util.bytes(user.getSalt());
        String realmName  =  this.getName();
//        System.out.println("realmName:"+realmName); com.cy.pj.sys.service.realm.ShiroRealm_0
        SimpleAuthenticationInfo info =
                new SimpleAuthenticationInfo(user,//principal用户身份(基于业务设置)
                user.getPassword(),//hashedCredentials 已加密的凭证（密码）
                credentialsSalt,//credentialsSalt(底层做了编码处理的加密盐对象)
                realmName//realmName
                );

        return info;
    }


    /**重写此方法的目的是，底层对用户输入的登录密码进行加密，需要算法*/
    @Override
    public CredentialsMatcher getCredentialsMatcher() {
        HashedCredentialsMatcher matcher=new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("MD5");
        matcher.setHashIterations(1);
        return matcher; //若没有返回，则也会出现IncorrectCredentialsException异常
    }

    //也可以在构造方法中通过调用set方法设置加密策略
    //    public ShiroRealm(){
    //        HashedCredentialsMatcher credentialsMatcher=
    //                new HashedCredentialsMatcher("MD5");
    //        credentialsMatcher.setHashIterations(1);
    //        setCredentialsMatcher(credentialsMatcher);
    //    }



@Autowired
private SysMenuDao sysMenuDao;

    /**此方法负责获取并封装授权信息*/
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principalCollection) {
        System.out.println("==doGetAuthorizationInfo==");
        //获取登录用户
        SysUser sysUser = (SysUser) principalCollection.getPrimaryPrincipal();
        //基于登录用户id，查询用户权限
        Set<String> permission =
                sysMenuDao.selectUserPermissions(sysUser.getId());
//        if(permission==null||permission.isEmpty())
//            throw  new AuthorizationException();

        //封装用户权限信息
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setStringPermissions(permission);

        return info;

    }


}
