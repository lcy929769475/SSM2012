package com.cy.pj.sys.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 基于此dao对象操作用户角色关系表数据
 */
@Mapper
public interface SysUserRoleDao {

    /**
     * 基于用户id删除用户角色关系数据
     * @param id
     * @return
     */
    @Delete("delete from sys_user_roles where user_id=#{id}")
    int  deleteByUserId(Integer id);

    /**
     * 基于用户id获取角色id
     * @param userId
     * @return
     */
    @Select("select role_id from sys_user_roles where user_id=#{userId}")
    List<Integer>  selectRoleIdsByUserId(Integer userId);

    /**
     * 新增用户和角色关系数据
     * @param userId 用户id
     * @param roleIds 角色id
     * @return
     */
    int insertUserRoles(Integer userId, List<Integer> roleIds);
}
