package com.cy.pj.sys.service.impl;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.common.util.ShiroUtil;
import com.cy.pj.sys.dao.SysUserDao;
import com.cy.pj.sys.dao.SysUserRoleDao;
import com.cy.pj.sys.pojo.SysUser;
import com.cy.pj.sys.service.SysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class SysUserServiceImpl implements SysUserService {
//    @Autowired
    private SysUserDao sysUserDao;
//    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    public SysUserServiceImpl(SysUserDao sysUserDao, SysUserRoleDao sysUserRoleDao) {
        this.sysUserDao = sysUserDao;
        this.sysUserRoleDao = sysUserRoleDao;
    }

//    @RequiresPermissions("sys:user:view")
    @RequiredLog(operation = "日志分页查询")
    @Override
    public List<SysUser> findUsers(SysUser entity) {
        return sysUserDao.selectUsers(entity);
    }

    //    @RequiresPermissions("sys:user:view")
//    @Override
//    public PageInfo<SysUser> findUsers(SysUser sysUser) {
//       return  PageUtil.startPage().doSelectPageInfo(()->{
//            sysUserDao.selectUsers(sysUser);
//        });
//        //return sysUserDao.selectUsers(sysUser);
//    }


    @Override
    public SysUser findById(Integer id) {
        //基于id查询用户以及用户对应部门信息
        SysUser sysUser = sysUserDao.selectById(id);//user，dept
        if(sysUser==null)
            throw new ServiceException("此用户不存在");
        //基于id查询用户对应的角色id
        List<Integer> roleIds = sysUserRoleDao.selectRoleIdsByUserId(id);//roles
        sysUser.setRoleIds(roleIds);
        return sysUser;
    }

    /**
     * @Transactional 描述方法时，此方法为一个事务切入点方法
     * @RequiresPermission 描述方法时，此方法为一个授权切入点方法，我们在访问此方法时，
     * 就需要授权，有权限可以访问，没有权限则抛出异常，那如何判定用户有没有访问此方法的权限呢？
     * 当我们在访问此方法时，shiro框架会获取此方法上的 @RequiresPermissions 注解，
     * 进而取到注解中的权限标识，然后调用subject对象的checkPermission(权限标识)方法检查用户是否有权限。
     *
     * 这个方法的权限检测调用流程分析？
     * controller -> AopProxy -> AuthorizingMethodInterceptor
     * ->subject.checkPermission->SecurityManager.CheckPermission->Authorize->Realm
     *
     * */

    @Override
    public int validById(Integer id, Integer valid) {
        //获取用户权限Set<String> permission
        //获取此方法上的权限标识：sys:user:update
        //判定用户的权限中是否包含方法上的权限标识 flag = permission.contains(sys:user:update)
        //if(flag){} else{}
        //String modifiedUser = "admin";//后续是登录用户
        //获取登录用户
        //SysUser user=(SysUser)SecurityUtils.getSubject().getPrincipal();
        SysUser user = ShiroUtil.getUser();
        String modifiedUser = user.getUsername();
        System.out.println("modifiedUser="+modifiedUser);

//      String modifiedUser = sysUserDao.selectById(id).getModifiedUser();
//        String modifiedUser="admin";//后续是登录用户


        int rows = sysUserDao.validById(id,valid,modifiedUser);
        if(rows==0)
            throw new ServiceException("记录可能不存在");
        return rows;
    }


    @Override
    public int saveUser(SysUser entity) {
        //产生一个随机字符串作为加密盐使用
        String password =entity.getPassword();
        String salt= UUID.randomUUID().toString();
        //对密码进行 MD5 盐值加密(MD5 特点:不可逆,相同内容加密结果也相同)  hashIterations，代表迭代次数
        SimpleHash sh = new SimpleHash("MD5",password,salt,1);
        String hashedPassword =sh.toHex();
        entity.setSalt(salt);
        entity.setPassword(hashedPassword);

        int rows = sysUserDao.insertUser(entity);
        sysUserRoleDao.insertUserRoles(entity.getId(),entity.getRoleIds());

        return rows;
    }


    @Override
    public int updateUser(SysUser entity) {
        //更新用户自身信息
        int rows = sysUserDao.updateUser(entity);
        if(rows==0)
            throw new ServiceException("记录可能不存在");
        //更新用户和角色关系数据
        sysUserRoleDao.deleteByUserId(entity.getId());
        sysUserRoleDao.insertUserRoles(entity.getId(), entity.getRoleIds());
        return rows;
    }
}
