package com.cy.pj.sys.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 * 部门PO对象
 */
@Data
public class SysDept implements Serializable{
	private static final long serialVersionUID = 6034162709372776674L;
	private Integer id;
	private String name;
	private Integer parentId;
	private Integer sort;
	private String remark;
	private Date createdTime;
	private Date modifiedTime;
	private String createdUser;
	private String modifiedUser;
}
