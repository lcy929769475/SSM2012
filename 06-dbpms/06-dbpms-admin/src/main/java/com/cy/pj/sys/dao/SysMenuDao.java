package com.cy.pj.sys.dao;

import com.cy.pj.common.pojo.Node;
import com.cy.pj.sys.pojo.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * 菜单数据访问层接口设计
 */
@Mapper
public interface SysMenuDao {
    /**
     * 查询所有菜单，包含菜单对应的上级菜单名称
     * @return 返回查询到的菜单信息
     */
    List<SysMenu> selectMenus();

    /**
     * 查询树节点信息（菜单id，名称，上级id），在添加或编辑菜单时，
     * 会以树结构方式呈现可选的上级菜单信息
     * @return
     */
    @Select("select id,name,parentId from sys_menus")
    List<Node> selectMenuTreeNodes();

    /**
     * 基于菜单id查询菜单信息
     * @param id 菜单id
     * @return 菜单记录信息
     */
    SysMenu selectById(Integer id);

    /**
     /**新增一条菜单信息
     * @param menu 封装了菜单信息的对象
     * @return 表示新增的行数
     * 对象此过程我们称之为持久化，将内存中的对象保存到数据库进行持久存储。
     * ORM：对象关系映射(对象和表之间的关系映射)
     */
    int insertMenu(SysMenu menu);

    /**
     * 更新菜单信息，将用户修改的内容更新到数据库
     * @param menu
     * @return 更新的行数
     */
    int updateMenu(SysMenu menu);

    /**
     * 基于用户id查询用户可以访问的菜单授权标识
     * @param userId
     * @return
     */
    Set<String> selectUserPermissions(Integer userId);

}
