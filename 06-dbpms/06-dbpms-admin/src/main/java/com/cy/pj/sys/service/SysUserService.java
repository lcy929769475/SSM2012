package com.cy.pj.sys.service;

import com.cy.pj.sys.pojo.SysUser;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SysUserService {
    /**
     * 基于条件查询用户以及用户对应的部门id,部门名称
     * @param entity
     * @return
     */
    List<SysUser> findUsers(SysUser entity);

    /**
     * 基于id查询用户以及用户对应部门，角色信息，当我们执行更新操作时，
     * 会基于id将其记录查询出来，然后更新到页面上。
     * @param id
     * @return
     */
    SysUser findById(Integer id);

    /**
     * 基于用户id更新用户状态，执行禁用启动操作
     * @param id 用户id
     * @param valid 状态
     * @return 更新行数
     */
    int validById(Integer id,Integer valid);

    /**
     * 新增用户，在添加用户时不仅仅要保存用户信息，还要保存用户和角色关系数据
     * @param entity
     * @return
     */
    int saveUser(SysUser entity);

    /**
     * 更新用户信息，在更新时不仅要更新用户自身信息，还要更新用户和角色关系数据
     * @param entity
     * @return 更新的行数
     */
    int updateUser(SysUser entity);
}
