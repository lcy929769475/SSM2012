package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbpmsUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbpmsUiApplication.class, args);
    }

}
