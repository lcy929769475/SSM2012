package com.cy.pj;

import com.cy.pj.sys.pojo.SysNotice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JSONTests {
    @Test
    void testJsonObject() throws JsonProcessingException {
        SysNotice notice = new SysNotice();
        notice.setId(100L);
        notice.setTitle("Study Json");
        notice.setContent("Json...");
        notice.setRemark("!!!!!!!");
        notice.setStatus("1");
        notice.setType("2");
        //基于jackson中的ObjectMapper类的实例将Pojo转换为json格式字符串
        String jsonString = new ObjectMapper().writeValueAsString(notice);
        System.out.println(jsonString);
        SysNotice notice2 = new ObjectMapper().readValue(jsonString, SysNotice.class);
        System.out.println(notice2);
    }
}
