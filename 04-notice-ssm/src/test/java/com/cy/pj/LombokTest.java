package com.cy.pj;

import com.cy.pj.sys.pojo.SysNotice;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@Slf4j
@SpringBootTest
public class LombokTest {

    // private static final Logger log= LoggerFactory.getLogger(LombokTest.class);

    @Test
    void testLog(){
        log.debug("test lombok log");
    }

    @Test
    void testNotice(){
        SysNotice n1 = new SysNotice(1L,"sss","1","1","1","s",new Date(),new Date(),"ss","ss");
        System.out.println(n1.toString());
    }
}
