package com.cy.pj.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

@SpringBootTest
public class JDBCTest {
    @Autowired
    private DataSource dataSource;

    @Test
    public void insert1() throws SQLException {
        Connection conn = dataSource.getConnection();

        String sql = "insert into sys_notices (title,content,type,status," +
                "createdTime,createdUser,modifiedTime,modifiedUser)" +
                "values('加课通知','加一节数学课','1','1',NOW(),'lcy',NOW(),'lcy')";

        Statement stmt =conn.createStatement();

        stmt.execute(sql);
        System.out.println("insert ok");
        stmt.close();
        conn.close();
    }

    @Test
    public void insert2() throws SQLException {
        Connection conn = dataSource.getConnection();

        String sql="insert into sys_notices (title,content,type,status," +
                "createdTime,createdUser,modifiedTime,modifiedUser)"+
                "values(?,?,?,?,?,?,?,?)";

        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, "紧急公告");
        pstmt.setString(2, "停水两天");
        pstmt.setString(3, "2");
        pstmt.setString(4, "1");
        pstmt.setTimestamp(5,new Timestamp(System.currentTimeMillis()));
        pstmt.setString(6, "lcy");
        pstmt.setTimestamp(7,new Timestamp(System.currentTimeMillis()));
        pstmt.setString(8, "lcy");

        pstmt.execute();
        pstmt.close();
        conn.close();
    }

    @Test
    void Select1() throws SQLException {
        //1.获取连接
        Connection conn = dataSource.getConnection();
        //2.创建Statement
        String sql = "select id,title,content,status,type,createdTime " +
                " from sys_notices where id>=?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        //3.发送sql
        pstmt.setInt(1, 1);
        //ResultSet rs=pstmt.executeQuery();
        boolean flag = pstmt.execute();
        ResultSet rs = null;
        if (flag) {
            rs = pstmt.getResultSet();
            //4.处理结果
            List<Map<String, Object>> list = new ArrayList<>();
            //Map<String,Object> map=new HashMap<>();
            while (rs.next()) {//行映射，循环一次取一行，一行记录一个map对象
                Map<String, Object> map = new LinkedHashMap<>();//LinkedHash 有顺序，Hash无顺序
                map.put("id", rs.getInt("id"));
                map.put("title", rs.getString("title"));
                map.put("content", rs.getString("content"));
                map.put("status", rs.getString("status"));
                map.put("type", rs.getString("type"));
                map.put("createdTime", rs.getTimestamp("createdTime"));
                //System.out.println(map);
                list.add(map);
            }
       for(Map<String,Object> m:list){
                System.out.println(m);
       }
        }
    }
    @Test
    void Select2 () throws SQLException {
        //1.获取连接
        Connection conn = dataSource.getConnection();
        //2.创建Statement
        String sql = "select id,title,content,status,type,createdTime " +
                " from sys_notices where id>=?";


        PreparedStatement pstmt = conn.prepareStatement(sql);
        //3.发送sql
        pstmt.setInt(1, 1);
        //ResultSet rs=pstmt.executeQuery();
        boolean flag = pstmt.execute();
        ResultSet rs = null;
        if (flag) {
            rs = pstmt.getResultSet();
            //获取结果集对应的元数据(描述数据的数据)
            ResultSetMetaData rsmd=rs.getMetaData();
            int columnCount=rsmd.getColumnCount();
            //4.处理结果
            List<Map<String, Object>> list = new ArrayList<>();
            //Map<String,Object> map=new HashMap<>();
            while (rs.next()) {//行映射，循环一次取一行，一行记录一个map对象
                Map<String, Object> map = new HashMap<>();
                for(int i=1;i<=columnCount;i++){//循环一次取一个字段值
                    //获取字段别名(没起别名，默认就是字段名)
                    String columnLabel=rsmd.getColumnLabel(i);
                    map.put(columnLabel,rs.getObject(columnLabel));
                }
//
                //System.out.println(map);
                list.add(map);
            }
            System.out.println(list);
        }
        rs.close();
        pstmt.close();
        conn.close();
    }




    @Test
    void xxx() throws SQLException {
        Connection conn = dataSource.getConnection();
        String sql = "select id,title,content,status,type,createdTime " +
                " from sys_notices where id>=?";
        PreparedStatement psmt = conn.prepareStatement(sql);
        psmt.setInt(1, 1);
        boolean flag = psmt.execute();
        ResultSet rs = null;
        if(flag){
            List<Map<String,Object>> list = new ArrayList<>();
            rs = psmt.getResultSet();
            ResultSetMetaData rsmt = psmt.getMetaData();
            int clm = rsmt.getColumnCount();
            while(rs.next()){
                Map<String,Object> map = new LinkedHashMap<>();//LinkedHashMap有顺序  HashMap无顺序
                for (int i = 1; i<=clm ; i++) {
                    String xx = rsmt.getColumnLabel(i);//getColumnLabel 包括自字段备注名字 ，  getColumnName只能包括字段原名
                    map.put(xx, rs.getObject(xx));
                }
                list.add(map);
            }
            for (Map<String,Object> o:list) {
                System.out.println(o);
            }
        }
        rs.close();
        psmt.close();
        conn.close();
    }
}
