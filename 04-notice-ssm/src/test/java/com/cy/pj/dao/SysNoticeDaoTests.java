package com.cy.pj.dao;

import com.cy.pj.sys.dao.SysNoticeDao;
import com.cy.pj.sys.pojo.SysNotice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SysNoticeDaoTests{

    @Autowired
    private SysNoticeDao sysNoticeDao;

    @Test
    void SelectNotices(){
        SysNotice notice=new SysNotice();
        notice.setTitle("公告");
        notice.setType("2");
        notice.setModifiedUser("lcy");
        List<SysNotice> list=sysNoticeDao.selectNotices(notice);
        for(SysNotice n:list){
            System.out.println(n);
        }
    }
    @Test
    void DeleteById(){
        int rows= sysNoticeDao.deleteById(8L,9L);
        System.out.println("rows="+rows);
    }

    @Test
    void UpdateNotice(){
        SysNotice notice=sysNoticeDao.selectById(6L);
        notice.setType("1");
        notice.setContent("2021/07/09 春节假期");
        notice.setModifiedUser("json");
        //将更新以后的内容持久化到数据库
        sysNoticeDao.updateNotice(notice);
    }

    @Test
    void SelectById(){
        SysNotice notice=sysNoticeDao.selectById(1L);
        System.out.println(notice);
    }

    @Test
    void InsertNotice(){
        //创建SysNotice对象,通过此对象封装要写入到数据库的数据
        SysNotice notice=new SysNotice();
        notice.setTitle("CGB2012结课时间");
        notice.setContent("2021/3/20正式结课");
        notice.setStatus("0");
        notice.setType("1");
        notice.setCreatedUser("tony");
        notice.setModifiedUser("tony");
        //将SysNotice对象持久化到数据库
        sysNoticeDao.insertNotice(notice);//此方法的实现内部会通过SQLSession向表中写数据。
    }
}
