package com.cy.pj.service;

import com.cy.pj.sys.pojo.SysNotice;
import com.cy.pj.sys.service.SysNoticeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SysNoticeServiceTests {
    @Autowired
    private SysNoticeService sysNoticeService;

    @Test
    void findNotices(){
        SysNotice notice = new SysNotice();
        notice.setType("1");
        notice.setModifiedUser("lcy");
        List<SysNotice> list = sysNoticeService.findNotices(notice);
//        for(SysNotice o:list){
//            System.out.println(o);
//        }
        list.forEach( item-> System.out.println(item));//jdk8 Lambda
    }
}
