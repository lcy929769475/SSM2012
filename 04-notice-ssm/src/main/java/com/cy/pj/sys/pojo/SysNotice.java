package com.cy.pj.sys.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data  //setter getter toString hashcode equals
@NoArgsConstructor
@AllArgsConstructor
public class SysNotice {//SysNotice.class
    /** 公告 ID */
    private Long id;
    /** 公告标题 */
    private String title;
    /** 公告类型（1 通知 2 公告） */
    private String type;
    /** 公告内容 */
    private String content;
    /** 公告状态（0 正常 1 关闭） */
    private String status;
    /** 备注*/
    private String remark;
    /** 创建时间
     * @DateTimeFormat 注解用于描述属性或set方法，用于告诉spring web模块，
     * 再将日期字符串转换为日期对象时，按照此注解中pattern属性设置进行转换
     * @JsonFormat 注解描述属性时，用于告诉底层API，在将对象转换为json字符串时，
     * 按照此注解中pattern属性指定的日期格式进行转换,其中timezone用于指定时区。
     * */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//默认不用-  而是用/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdTime;
    /** 修改时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date modifiedTime;
    /** 创建用户 */
    private String createdUser;
    /** 修改用户*/
    private String modifiedUser;
    //自己添加 set/get/toString 方法

}
