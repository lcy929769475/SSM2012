package com.cy.pj.sys.service;

import com.cy.pj.sys.pojo.SysNotice;

import java.util.List;


/**
 * 基于此接口定义公告模块业务规范
 * 1）添加公告
 * 2）修改公告
 * 3）删除公告
 * 4）增加公告
 */
public interface SysNoticeService {
    /**
     * 新增一条公告信息
     * @param notice 封装了要新增的公告信息
     * @return 添加的行数
     */
    int saveNotice(SysNotice notice);

    /**
     * 基于id删除公告信息
     * @param ids 删除公共的id
     * @return 删除的行数
     */
    int deleteById(Long... ids);

    /**
     * 基于id修改一条公告信息
     * @param notice 封装了要修改的公告信息
     * @return 修改的行数
     */
    int updateNotice(SysNotice notice);

    /**
     * 基于id查询公告信息
     * @param id 公告的id
     * @return 具体的公告信息
     */
    SysNotice findById(Long id);

    /**
     * 基于条件查询公告信息
     * @param notice 封装了查询条件
     * @return 查询到的公告信息
     */
    List<SysNotice> findNotices(SysNotice notice);



}
