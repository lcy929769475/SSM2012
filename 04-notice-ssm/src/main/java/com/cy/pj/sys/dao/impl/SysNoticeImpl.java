package com.cy.pj.sys.dao.impl;

import com.cy.pj.sys.dao.SysNoticeDao;
import com.cy.pj.sys.pojo.SysNotice;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


//当不使用@Mapper时候自己写的Dao实现类来实现方法,当使用@Mapper时不实现这个实现类。
//@Repository //默认bean名字为类名，首字母小写
public class SysNoticeImpl implements SysNoticeDao {

    @Autowired
    private SqlSession sqlSession;

    @Override
    public List<SysNotice> selectNotices(SysNotice notice) {
//        String  Statement= "com.cy.pj.sys.dao.SysNoticeDao.selectNotices";
        //如何不自己拼接key来写mapping映射的key值？
          String interfaceName=this.getClass().getInterfaces()[0].getName();
          System.out.println("interfaceName="+ interfaceName);
          String elementId=Thread.currentThread().getStackTrace()[1].getMethodName();
          String  Statement = interfaceName + "."+elementId;
          System.out.println("Statement="+Statement);
        List<SysNotice> list = sqlSession.selectList(Statement,notice);
        return list;
    }





    @Override
    public int deleteById(Long... ids) {
        return 0;
    }

    @Override
    public int updateNotice(SysNotice notice) {
        return 0;
    }

    @Override
    public int insertNotice(SysNotice notice) {
        return 0;
    }

    @Override
    public SysNotice selectById(Long id) {
        return null;
    }
}
