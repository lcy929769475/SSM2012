package com.cy.pj.sys.service.impl;


import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.sys.dao.SysNoticeDao;
import com.cy.pj.sys.pojo.SysNotice;
import com.cy.pj.sys.service.SysNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 公共业务的具体实现
 * 1）核心业务
 * 2）拓展业务（日志记录，权限控制，事务控制，缓存，...）
 */

@Service//@Component
public class SysNoticeServiceImpl implements SysNoticeService {
    //初始化日志对象（org.slf4j）
    private static final Logger log =
            LoggerFactory.getLogger(SysNoticeServiceImpl.class);//通过工厂创建日志对象

    //has a
    //@Autowired  //假如此位置没写这个注解 ，则可以通过构造方法的形式为属性赋值
    private SysNoticeDao sysNoticeDao;

    //通过构造方法直接进行DI操作 @Autowired 可以省略
    public SysNoticeServiceImpl(SysNoticeDao sysNoticeDao){
        this.sysNoticeDao=sysNoticeDao;
    }



    @Override
    public List<SysNotice> findNotices(SysNotice notice) {
//        long t1 =System.currentTimeMillis();
//        log.debug("method start: {}",t1);
        //这里的{}表示占位符号
        List<SysNotice> list=sysNoticeDao.selectNotices(notice);
//        long t2 = System.currentTimeMillis();
//        log.debug("end: {} {}",t2,"!!");
//        log.info("time: {}",t2-t1);
        return list;

    }

    @Override
    public int saveNotice(SysNotice notice) {
        int rows = sysNoticeDao.insertNotice(notice);
        return rows;
    }

    @Override
    public int deleteById(Long... ids) {
        int rows = sysNoticeDao.deleteById(ids);
        if (rows==0)
            throw new ServiceException("记录可能已经不存在了");
        return rows;
    }

    @Override
    public int updateNotice(SysNotice notice) {
        int rows = sysNoticeDao.updateNotice(notice);
        return rows;
    }

    @Override
    public SysNotice findById(Long id) {
        SysNotice notice = sysNoticeDao.selectById(id);
        return notice;
    }


}
