package com.cy.pj.sys.web.controller;

import com.cy.pj.common.util.PageUtil;
import com.cy.pj.sys.pojo.SysNotice;
import com.cy.pj.sys.service.SysNoticeService;
import com.cy.pj.sys.web.pojo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 此对象为公告模块控制层处理器对象
 * 1.请求url,方式设计
 * 1)条件查询(请求方式-Get,请求url-"/notice/")
 * 2)新增公告(请求方式-Post,请求url-"/notice/")
 * 3)基于id查询(请求方式-Get,请求url-"/notice/{id}")
 * 4)更新公告(请求方式-Put，请求url-"/notice/")
 * 5)删除公告(请求方式-Delete,请求url-"/notice/{id}")
 *
 * 2.请求参数设计
 * 1)参数传递方式
 * 1.1)将请求参数通过url进行传递
 * 1.1.1)http://ip:port/?title=my&type=1
 * 1.1.2)http://ip:port/10
 * 1.2)请求参数通过请求体进行传递
 * 1.2.1) {"title":"Study Spring Boot","Content":"Study ....",....}
 *
 * 1.2)服务端接收请求参数
 * 1.2.1)直接量(8种基本数据类型，字符串，日期)
 * 1.2.2)pojo对象(但是需要提供和参数名对应的set方法)
 * 1.2.3)map对象(前提是必须使用@RequestParam或@RequestBody注解对参数进行描述)
 *
 * 1.3)方法参数描述
 * 1.3.1)@PathVariable 用于获取请求url中{}内部的变量
 * 1.3.2)@RequestBody 用于获取请求体中json格式的数据
 * 1.3.3)@RequestParam 用于获取非json格式的请求参数数据
 * 1.3.4)@DateTimeFormat 用于设置可以接收的日期格式字符串
 *
 * 2.响应标准设计 (状态，消息，数据)：JsonResult
 *
 */

//@Controller
//@ResponseBody
@RestController // 包含@Controller @ResponseBody
@RequestMapping("/notice/")
//@RequestMapping(Constants.NOTICE_ROOT)
public class SysNoticeController {

    @Autowired
    private SysNoticeService sysNoticeService;

    /**
     * 处理客户端的查询请求
     * 不带参数的请求url：  http://localhost/notice/
     * 带参数的请求url：    http://localhost/notice/?type=1&title=my
     */

    @GetMapping
    public JsonResult FindNotics(SysNotice notice){
        //启动分页查询拦截
/*        return  new JsonResult(PageHelper.startPage(1, 3)
                .doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                sysNoticeService.findNotices(notice);
            }
        }));*/   //下面的是简化版

//        return  new JsonResult(PageHelper.startPage(1, 3)
//                //执行查询
//                .doSelectPageInfo(()->{
//                    sysNoticeService.findNotices(notice);
//                }));

        return new JsonResult(PageUtil.startPage().doSelectPageInfo(()-> {
            sysNoticeService.findNotices(notice);
        }));

        /*  return new JsonResult(PageUtil.startPage()
              .doSelectPageInfo(()->
            sysNoticeService.findNotices(sysNotice)));*/

//        return  new JsonResult(sysNoticeService.findNotices(notice)) ;

        //这个结果会返回给谁？调用方（DispatcherServlet）
        //方法调用方DispatcherServlet 拿到方法执行结果以后，
        //会调用相关api将结果转换为json格式字符串，最后通过Response对象，将字符串响应到客户端
    }


    /**
     * rest风格的url允许使用变量，但是这个变量需要使用{}括起来
     * 当我们在方法参数中需要这个变量的值时，可以使用@PathVariable 对方法参数进行描述
     * 访问url:http://localhost/notice/1
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public JsonResult FindById(@PathVariable Long id){
        //        try {
        return new JsonResult(sysNoticeService.findById(id));
        //        }catch (Exception e){
//            return new JsonResult(e);
//        }
    }

    /**
     * 新增公告信息
     * @param notice 封装客户端提交的通告信息，假如使用@RequestBody对参数
     * 进行描述，客户端可以向服务端传递json格式的字符串参数，服务端拿到字符串
     * 以后，会将json字符串转为参数类型的对象(这个过程还可以理解为json反序列化)
     *  注意：一个Controller方法中只能有一个参数使用@RequestBody注解进行描述
     * @return
     */
    @PostMapping
    public JsonResult saveNotice(@RequestBody SysNotice notice){
        sysNoticeService.saveNotice(notice);
        return new JsonResult("save ok");//json序列化(将对象转换成json格式字符串)
    }

    @PutMapping
    public JsonResult UpdateNotice(@RequestBody SysNotice notice){
        sysNoticeService.updateNotice(notice);
        return new JsonResult("update ok");
    }

    @DeleteMapping("{ids}")
    public JsonResult deleteNotice(@PathVariable Long... ids){
        sysNoticeService.deleteById(ids);
        return new JsonResult("delete ok");
    }


    //局部异常处理方法，只能处理当前controller中的RuntimeException异常
//    @ExceptionHandler(RuntimeException.class)
//    public JsonResult doHandleRuntimeException(RuntimeException e){
//        e.printStackTrace();
//        return new JsonResult(e);
//    }
}
