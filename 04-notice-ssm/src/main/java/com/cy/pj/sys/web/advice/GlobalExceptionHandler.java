package com.cy.pj.sys.web.advice;

import com.cy.pj.sys.web.pojo.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @RestControllerAdvice 注解描述的类，为spring web模块定义的全局异常处理类。
 * 当我们在@RestController/@Controller注解描述的类或父类中没有处理异常，则
 * 系统会查找@RestControllerAdvice注解描述的全局异常处理类，可以通过此类中的
 * 异常处理方法对异常进行处理。
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
//    private static final Logger log =
//            LoggerFactory.getLogger(GlobalExceptionHandler.class);


    /**
     * @ExceptionHandler 注解描述的方法为异常处理方法,
     * 注解中定义的异常类型为方法可以处理的异常类型（包含这个异常类型的子类类型）
     * @param e 此参数用于接收要处理的异常对象，通常会与@ExceptionHandler注解中
     *          定义的异常类型相同，或者是@ExceptionHandler注解中异常类型的父亲
     * @return 封装了异常状态和信息的对象
     */
    @ExceptionHandler(RuntimeException.class)
    public JsonResult doHandleRuntimeException(RuntimeException e){
        e.printStackTrace();
        log.error("exception msg is:{}",e.getMessage());
        return  new JsonResult(e);
    }
}
