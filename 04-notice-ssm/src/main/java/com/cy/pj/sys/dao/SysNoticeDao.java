package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysNotice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/***
 * 创建公告模块数据持久层对象，借助此类型的对象基于mybatis技术实现与数据库的交互
 * 通过这些交互来访问或操作公告模块的数据
 * 建议：在springboot工程中基于mybatis实现与数据库交互时，一般首先要定义一个数据层接口（例如SysNoticeDao），
 * 然后通过@Mapper注解堆接口进行描述，系统启动时，会对启动类所在包以及子包的类进行扫描，
 * 假如发现接口上有@Mapper注解（mybatis提供），系统底层会基于接口创建其实现类（借助反射包中Proxy类进行创建），
 * 在实现类的内部，底层会基于 sqlsession对象实现数据访问和操作。
 */
@Mapper
public interface SysNoticeDao {//系统底层会基于JDK中的Proxy API为此接口产生一个实现类
    /**
     * 基于条件查询通告信息
     * @param notice
     * @return 返回查询到的通告信息
     */
    List<SysNotice> selectNotices(SysNotice notice);

    /***
     * 基于多个id执行记录删除操作
     * @param ids 要删除的记录id，这里的语法为可变参数(可以看成特殊数组)
     * 可变参数主要是用于简化名字相同，参数类型也相同，但个数不同的这样的一系列方法的定义
     * @return 删除的行数
     * 说明：在jdk8之前，对于接口方法，假如参数有多个或者参数是数组，
     * 是不可以直接在sql映射文件中使用参数名的，需要通过@Param这个注解定义参数名，
     * 然后在sql映射语句中使用@Param注解中的名字获取参数数据，对于数组而言在sql映射中
     * 可以直接使用ids进行接受也可以
     */
    int deleteById(/*@Param("ids")*/ Long... ids);//1,2,3


    /**将内存中的notice对象，更新到数据库*/
    int updateNotice(SysNotice notice);

    /**将内存中的notice对象，持久化到数据库*/
    int insertNotice(SysNotice notice);

    /**
     * 基于id查询notice信息
     * @param id 查询条件
     * @return 查询到的notice对象(存储表中的一行记录)
     * 说明：简单的sql映射可以直接将sql语句写到方法上面，以注解进行声明
     */
    @Select("select * from sys_notices where id=#{id}")
    SysNotice  selectById(Long id);
}
